package chat.action;

import chat.Signup;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import javax.servlet.http.Cookie;
import java.sql.*;

public class SignupAction extends Action 
{
     String username = "";
     public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)throws Exception 
     {
          ActionMessages errorAdder=new ActionMessages();
             Signup obj=(Signup)form;
             if(getErrors(request) == null || getErrors(request).size() == 0)
             {
               Class.forName("com.mysql.jdbc.Driver");
               Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/Sky_spy","root","");
               PreparedStatement stm1 = con.prepareStatement("select username from Chat_Users;");
               ResultSet rs = stm1.executeQuery();
               while (rs.next())
               {
                    username=rs.getString(1);
                    if(obj.getUsername().equals(rs.getString(1)))
                    {
                         errorAdder.add("invalidUsername",new ActionMessage("errors.username.invalid"));
                         addErrors(request,errorAdder);
                         return mapping.getInputForward();
                    }
               }
               PreparedStatement stm = con.prepareStatement("insert into Chat_Users(Name,username,password,Gender,DOB,phonenumber) values(?,?,?,?,?,?)");
               stm.setString(1,obj.getName());
               stm.setString(2,obj.getUsername());
               stm.setString(3,obj.getPassword() );
               stm.setString(4,obj.getGender());
               stm.setString(5,obj.getDob());
               stm.setString(6,obj.getPhno());
               stm.executeUpdate();
               return mapping.findForward("success");
               }
		   else
		   {
			return mapping.getInputForward();
		   }
			
     }
}
