package Profile;

import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionForm;
public class ProfilePic extends ActionForm{
	
	private FormFile file;
	
	public void setFile(FormFile file) {
		this.file = file;
	}
	
	public FormFile getFile() {
		return file;
	}
}
