package SigninValidate;

import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

public class Signin extends ActionForm {
     private String usrname,passwd;
     
	public void setUsrname(String usrname) {
		this.usrname = usrname;
	}
	public String getUsrname() {
		return usrname;
	}
	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}
	public String getPasswd() {
		return passwd;
	}
	public ActionErrors validate(ActionMapping mapping,HttpServletRequest request) {
     ActionErrors errors =new ActionErrors();
     if(usrname == null || usrname.equals(""))
          errors.add("usrname",new ActionMessage("errors.usrname.required","usrname"));
     if(passwd == null || passwd.equals(""))
          errors.add("passwd",new ActionMessage("errors.passwd.required","Passwd"));
     return errors;
     }
     
     public void reset(ActionMapping mapping, HttpServletRequest request) {
		this.usrname = null;
		this.passwd = null;
		super.reset(mapping, request);
	}
}
