package chat;

import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

public class Signup extends ActionForm {
     private String name,username,password,confirmpassword,gender,dob,phno;
     
     public void setName(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getUsername() {
		return username;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPassword() {
		return password;
	}
	public void setConfirmpassword(String confirmpassword) {
		this.confirmpassword = confirmpassword;
	}
	public String getConfirmpassword() {
		return confirmpassword;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getGender() {
		return gender;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getDob() {
		return dob;
	}
	public void setPhno(String phno) {
		this.phno = phno;
	}
	public String getPhno() {
		return phno;
	}
public ActionErrors validate(ActionMapping mapping,HttpServletRequest request) {
     ActionErrors errors =new ActionErrors();
     if(name == null || name.equals(""))
          errors.add("name",new ActionMessage("errors.name.required","Name"));
     if(username == null || username.equals(""))
          errors.add("username",new ActionMessage("errors.username.required","Username"));
      if(confirmpassword == null || confirmpassword.equals(""))
          errors.add("confirmpassword",new ActionMessage("errors.confirmpassword.required","confirmpassword"));
     if(gender == null || gender.equals(""))
          errors.add("gender",new ActionMessage("errors.gender.required","gender"));
     if(dob == null || dob.equals(""))
          errors.add("dob",new ActionMessage("errors.dob.required","dob"));
      if(phno == null ||  phno.equals(""))
          errors.add("phno",new ActionMessage("errors.phno.required","phno"));     
      if(password == null || password.equals(""))
          errors.add("password",new ActionMessage("errors.password.required","Password"));
     else if(!password.equals(confirmpassword))
          errors.add("password",new ActionMessage("errors.password.invalid","Password"));
     return errors;
}
}
