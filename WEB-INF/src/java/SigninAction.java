package SigninValidate.action;

import SigninValidate.Signin;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.action.ActionErrors;
import java.sql.*;

public class SigninAction extends Action 
{
     public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)throws Exception 
     {
     PreparedStatement stm1 = null;
     Connection con = null;
     ActionMessages errorAdder=new ActionMessages();
     Signin obj=(Signin)form;
     try
     {
          
          if(getErrors(request) == null || getErrors(request).size() == 0)
             {   
              Class.forName("com.mysql.jdbc.Driver");
               con = DriverManager.getConnection("jdbc:mysql://localhost:3306/Sky_spy","root","");
               stm1 = con.prepareStatement("select username,password from Chat_Users where username like \""+obj.getUsrname()+"\" ;");
               ResultSet rs = stm1.executeQuery();
               if(rs.next())
               {
                    if(obj.getUsrname().equals(rs.getString("username")) && obj.getPasswd().equals(rs.getString("password")) )
                    {
                             return mapping.findForward("success");
                   }
                    errorAdder.add("invalidpasswd",new ActionMessage("errors.passwd.invalid"));
                    addErrors(request,errorAdder);
                   return mapping.getInputForward();
		  }
          }  
     errorAdder.add("invalidUsrname",new ActionMessage("errors.usrname.invalid"));
     addErrors(request,errorAdder);
     }
     catch(Exception e)
     {
     errorAdder.add("invalidUsrname",new ActionMessage("errors.usrname.invalid"));
      addErrors(request,errorAdder);
		e.printStackTrace();
     }
     finally
     {
     	if(con != null)
     	{
     		con.close();
     	}
     	if(stm1 != null)
     	{
     		stm1.close();
     	}
     }
     return mapping.getInputForward();
     }
}
