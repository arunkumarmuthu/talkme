drop database if exists Sky_spy;

create database Sky_spy;

use Sky_spy;

create table Chat_Users(id int primary key Auto_increment,Name varchar(50),username varchar(30) UNIQUE,password varchar(30),Gender varchar(20),DOB varchar(30),phonenumber varchar(20));

create table Friends(id int primary key Auto_increment,User1 int references Chat_Users(id),User2 int references Chat_Users(id));

create table Requests(id int primary key Auto_increment,Sent_By int references Chat_Users(id),Sent_To int references Chat_Users(id),Is_Accept ENUM('Yes','No'));

create table Sessions(id int primary key auto_increment, Sess_id int references Requests(id),Start_time DATETIME ,finish_time DATETIME);

create table Messages(id int primary key Auto_increment, Chat_Id int references Friends(id),S_id int references Chat_Users(id),R_id int references Chat_Users(id),msg LONGTEXT,Msg_Time datetime);

;#create table Groups(id int primary key Auto_increment,Name varchar(50),Admin int references Chat_Users(id),Create_Time datetime);

;#create table Group_Members(id int primary key Auto_increment,G_Id int references Groups(id),Mem_Id int references Chat_Users(id)); 


