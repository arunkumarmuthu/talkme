<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ taglib prefix= "html" uri="http://struts.apache.org/tags-html" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="css/signup.css">
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
  <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
  <script>
  $(function() {
    $( "#datepicker" ).datepicker();
  });
  
  </script>
  
</head>
<body>
	<div id = "header"><img src="images/logo1.png" id="logo">ZuChat</div>
          <div id="main" class="rain">
          <html:form action="signup.do" >
          <p>
               
           <label for="login">Name:</label>
           <html:text name="signupForm" property="name" styleClass="login"/>
           <html:errors property="name" />
         </p>
         
         <p>
           <label for="login">User Name:</label>
           <html:text name="signupForm" property="username" styleClass="login" />
           <html:errors property="username"/>
           <html:errors property="invalidUsername"/>
         </p>
         
         <p>
           <label for="login">Password:</label>
           <html:password name="signupForm" property="password" styleClass="login"/>
           <html:errors property="password"/>
         </p>
         
         <p>
           <label for="login">confirmpwd:</label>
           <html:password name="signupForm" property="confirmpassword" styleClass="login"/>
           <html:errors property="confirmpassword"/>
         </p>
         
         <p>
           <label for="login">Gender:</label>
           <html:select name="signupForm" property="gender" styleClass="login">
               <html:option value="">---select---</html:option>
	          <html:option value="male">male</html:option>
	          <html:option value="female">female</html:option>
	          <html:option value="others">others</html:option>
          </html:select>
           <html:errors property="gender"/>
         </p>
         
         <p>
           <label for="login">DOB:</label>
           <html:text name="signupForm" property="dob" styleClass="login" styleId="datepicker"/>
           <html:errors property="dob"/>
         </p>
         
         <p>
           <label for="login">Ph number:</label>
           <html:text name="signupForm" property="phno" styleClass="login"/>
           <html:errors property="phno"/>
         </p>
         
          <p class="login-submit">
               <button type="submit" class="login-button">Login </button>
          </p>
         </html:form>
         </div>
</body>
</html>
