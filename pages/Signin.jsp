<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
     pageEncoding="ISO-8859-1"%>
 <%@ taglib prefix= "html" uri="http://struts.apache.org/tags-html" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>Login</title>
  <link rel="stylesheet" href="css/signin.css">
</head>
<body>
     <div id="headerDiv"><div id = "headerText">talk me</div>
     <a href = "pages/Signup.jsp" id><input type="button" id = "signup-btn" value = "SignUp" /></a></div>
     <div id="outerdiv">
     <h2 id="h2">talk me login<h2>
     <hr>
     <div id="leftside"><img src="img/profile.png" class="imgClass" /></div>
     <div></div>
  <html:form  action="signin.do" styleClass="login">
    <p>
      <label for="login">User Name:</label>
      <html:text  property="usrname" styleId="login" />
    </p>
    <p>
      <label for="login">Password:</label>
      <html:password  property="passwd" styleId="password"/>
      <html:errors />
    </p>

    <p class="login-submit">
      <button type="submit" class="login-button">Login</button>
    </p>
    <div>
     </html:form>
</body>
</html>
